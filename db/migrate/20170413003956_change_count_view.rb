class ChangeCountView < ActiveRecord::Migration[5.0]
  def up
    execute <<~SQL
      DROP VIEW count_data
    SQL

    execute <<~SQL
      CREATE VIEW count_data AS
        SELECT 
          idol_id,
          tweet_count,  
          user_count
        FROM idols
          LEFT OUTER JOIN (
            SELECT
              idol_id,
              count(*) AS tweet_count,
              count(DISTINCT user_id) AS user_count
            FROM extracted_tweets
            WHERE source = '<a href="http://sp.pf.mbga.jp/12008305" rel="nofollow">アイドルマスター シンデレラガールズ公式</a>'
            GROUP BY idol_id
          ) tw_counts ON idols.id = tw_counts.idol_id
    SQL
  end

  def down
    execute <<~SQL
      DROP VIEW count_data
    SQL

    execute <<~SQL
      CREATE VIEW count_data AS
        SELECT 
          idol_id,
          tweet_count,  
          user_count
        FROM idols
          LEFT OUTER JOIN (
            SELECT
              idol_id,
              count(*) AS tweet_count,
              count(DISTINCT user_id) AS user_count
            FROM extracted_tweets
            GROUP BY idol_id
          ) tw_counts ON idols.id = tw_counts.idol_id
    SQL
  end
end
