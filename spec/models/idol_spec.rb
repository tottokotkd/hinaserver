require 'rails_helper'

RSpec.describe Idol, type: :model do
  specify 'tweet_count to return correct values' do
    idol = Idol.find 1
    user = User.create! id: 123

    Tweet.create! id: 1, user: user, idol: idol, source_checked: true
    Tweet.create! id: 2, user: user, idol: idol, source_checked: true
    Tweet.create! id: 3, user: user, idol: idol, source_checked: false
    Tweet.create! id: 4, user: user, idol: nil, source_checked: true

    a_user = User.create! id: 456
    a_idol = Idol.find 2

    Tweet.create! id: 5, user: a_user, idol: idol, source_checked: true
    Tweet.create! id: 6, user: a_user, idol: a_idol, source_checked: true
    Tweet.create! id: 7, user: a_user, idol: idol, source_checked: false
    Tweet.create! id: 8, user: a_user, idol: nil, source_checked: true
    Tweet.create! id: 9, user: a_user, idol: idol, source_checked: true

    expect(idol.tweet_count(true)).to eq 4
    expect(Idol.tweet_count_map[1]).to eq idol.tweet_count(true)
  end

  specify 'user_count to return correct values' do
    idol = Idol.find 1
    user = User.create! id: 123

    Tweet.create! id: 1, user: user, idol: idol, source_checked: true
    Tweet.create! id: 2, user: user, idol: idol, source_checked: true
    Tweet.create! id: 3, user: user, idol: idol, source_checked: false
    Tweet.create! id: 4, user: user, idol: nil, source_checked: true

    a_user = User.create! id: 456
    a_idol = Idol.find 2

    Tweet.create! id: 5, user: a_user, idol: idol, source_checked: true
    Tweet.create! id: 6, user: a_user, idol: a_idol, source_checked: true
    Tweet.create! id: 7, user: a_user, idol: idol, source_checked: false
    Tweet.create! id: 8, user: a_user, idol: nil, source_checked: true
    Tweet.create! id: 9, user: a_user, idol: idol, source_checked: true

    expect(idol.user_count(true)).to eq 2
    expect(Idol.user_count_map[1]).to eq idol.user_count(true)
  end
end
