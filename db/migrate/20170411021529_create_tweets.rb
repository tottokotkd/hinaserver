class CreateTweets < ActiveRecord::Migration[5.0]
  def change

    create_table :idols do |t|
      t.string :name, index: true, null: false
      t.string :yomi, index: true, null: false
      t.timestamps
    end

    create_table :tweets, id: false do |t|
      t.string :id, primary_key: true
      t.references :idol, foreign_key: true
      t.text :json
      t.timestamps
    end

    create_table :search_records do |t|
      t.datetime :since
      t.datetime :until
      t.string :max_id
      t.boolean :done
      t.timestamps
    end

  end
end
