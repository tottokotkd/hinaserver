require 'twitter'
require 'json'

class Tweet < ApplicationRecord

  belongs_to :idol, required: false
  belongs_to :user, required: false

  scope :valid_tweets, -> { where(source_checked: true).where.not(idol: nil) }

  def json_hash
    JSON.parse json
  end

end
