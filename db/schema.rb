# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170414134524) do

  create_table "idols", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "yomi",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_idols_on_name"
    t.index ["yomi"], name: "index_idols_on_yomi"
  end

  create_table "search_records", force: :cascade do |t|
    t.datetime "since"
    t.datetime "until"
    t.string   "max_id"
    t.boolean  "done"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "cached",     default: false
  end

  create_table "tweets", force: :cascade do |t|
    t.integer  "idol_id"
    t.integer  "user_id",        limit: 8
    t.boolean  "source_checked",           default: false, null: false
    t.text     "json"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["created_at"], name: "index_tweets_on_created_at"
    t.index ["id"], name: "sqlite_autoindex_tweets_1", unique: true
    t.index ["idol_id"], name: "index_tweets_on_idol_id"
    t.index ["source_checked", "idol_id", "user_id"], name: "index_tweets_on_source_checked_and_idol_id_and_user_id"
    t.index ["source_checked"], name: "index_tweets_on_source_checked"
    t.index ["user_id"], name: "index_tweets_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "screen_name"
    t.string   "user_name"
    t.string   "profile_image_url"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["id"], name: "sqlite_autoindex_users_1", unique: true
  end

end
