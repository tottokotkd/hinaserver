class TopController < ApplicationController

  def index
    @idols = Rails.cache.fetch "TopController/index/#{SearchRecord.latest_update_timestamp}/#{sort_target}/#{sort_order}" do
      Idol.all_idols.sort do |a, b| sort_handler(a, b) * (sort_order == :asc ? 1 : -1) end
    end

    # meta tags
    set_meta_tags twitter: {
      card: 'summary',
      site: '@tottokotkd',
      title: '全自動でツイート集計したやつ',
      description: '15分毎に自動更新するけど何回もアクセスしないで (サーバーが死ぬ)',
    }

  end

  def update
    begin
      SearchRecord.update
    rescue => e
      puts e
    end

    redirect_to action: 'index'
  end

  private

  def sort_target
    keys =%w(id name tweet account tw_p_a)
    keys.include?(params[:sort]) ? params[:sort].to_sym : :name
  end

  def sort_order
    params[:order].eql?('desc') ? :desc : :asc
  end

  def sort_handler(a, b)
    case sort_target
      when :name
        a.display_yomi <=> b.display_yomi
      when :tweet
        a.tweet_count <=> b.tweet_count
      when :account
        a.user_count <=> b.user_count
      when :tw_p_a
        a.tpu_count <=> b.tpu_count
      else
        a.id <=> b.id
    end
  end

  def count_chart
    dates = SearchRecord.start_of_campaign.to_date...SearchRecord.latest_update_timestamp.to_date
    tweets_by_day = dates.map do |date| Idol.tweet_of_date date end
    tweets_until_day = dates.map do |date| Idol.tweet_until_date date end
    users_by_day = dates.map do |date| Idol.user_of_date date end
    users_until_day = dates.map do |date| Idol.user_until_date date end

    LazyHighCharts::HighChart.new('graph') do |f|
      f.title(text: 'ツイート・アカウント数')
      f.xAxis(categories: dates.map{ |d| d.to_time.to_s(:short_md)})
      f.series(name: '1日の増加ツイート数', yAxis: 1, data: tweets_by_day, type: 'column')
      f.series(name: '1日の増加アカウント数', yAxis: 1, data: users_by_day, type: 'column')
      f.series(name: '合計ツイート数', yAxis: 0, data: tweets_until_day)
      f.series(name: '合計アカウント数', yAxis: 0, data: users_until_day)
      f.yAxis(
        [
          { title: { text: '合計' }, opposite: true, min: 0, max: 300000 },
          { title: { text: '1日あたり'}, margin: 10, min: 0, max: 15000 },
        ])
    end
  end

  helper_method %i(sort_target sort_order count_chart)
end
