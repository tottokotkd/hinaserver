class ChangeColumnTypeOfTweet < ActiveRecord::Migration[5.0]
  def up

    create_table :users_tmp, id: false do |t|
      t.string :id, primary_key: true
      t.string :screen_name
      t.string :user_name
      t.string :profile_image_url
      t.timestamps
    end

    execute <<~SQL
      INSERT INTO users_tmp (id, screen_name, user_name, profile_image_url, created_at, updated_at)
      SELECT id, screen_name, user_name, profile_image_url, created_at, updated_at from users
    SQL

    create_table :tweets_tmp, id: false do |t|
      t.string :id, primary_key: true
      t.references :idol, foreign_key: true
      t.text :json
      t.timestamps
      t.integer :user_id
    end

    execute <<~SQL
      INSERT INTO tweets_tmp (id, idol_id, user_id, json, created_at, updated_at)
      SELECT id, idol_id, user_id, json, created_at, updated_at from tweets
    SQL

    drop_table :tweets
    drop_table :users

    create_table :users, id: false do |t|
      t.integer :id, primary_key: true, limit: 8
      t.string :screen_name
      t.string :user_name
      t.string :profile_image_url
      t.timestamps
    end

    execute <<~SQL
      INSERT INTO users (id, screen_name, user_name, profile_image_url, created_at, updated_at)
      SELECT id, screen_name, user_name, profile_image_url, created_at, updated_at from users_tmp
    SQL

    create_table :tweets, id: false do |t|
      t.integer :id, primary_key: true, limit: 8
      t.references :idol, foreign_key: true
      t.references :user, limit: 8, foreign_key: true
      t.boolean :source_checked, null: false, default: false
      t.text :json
      t.timestamps
    end

    execute <<~SQL
      INSERT INTO tweets (id, idol_id, json, created_at, updated_at)
      SELECT id, idol_id, json, created_at, updated_at from tweets_tmp
    SQL

    drop_table :tweets_tmp
    drop_table :users_tmp

  end
end
