class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users, id: false do |t|
      t.string :id, primary_key: true
      t.string :screen_name
      t.string :user_name
      t.string :profile_image_url

      t.timestamps
    end
  end
end
