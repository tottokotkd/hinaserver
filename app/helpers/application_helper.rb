module ApplicationHelper
  def link_idol(idol)
    link_to idol.display_name, controller: :idols, action: :show, id: idol.id
  end
end
